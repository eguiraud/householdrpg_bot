# A Telegram bot to roll dice for Household RPG

To use, add the bot to your channel and use the `/roll` or `/roll X` commands (where X is the grade of your ability).

You can find it at `@householdrpg_bot`.
