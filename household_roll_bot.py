from telegram.ext import Updater
import logging
from telegram.ext import ConversationHandler, CommandHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, ParseMode
from collections import defaultdict, Counter
import numpy.random as rand

MD = ParseMode.MARKDOWN

def button(txt):
    """Helper function to create InlineKeyboardButtons."""
    txt = str(txt)
    return InlineKeyboardButton(txt, callback_data=txt)

def validate_user(update):
    """Assert user that clicked on a button is the same we were talking to."""
    original_message = update.callback_query.message.reply_to_message
    original_user = original_message.from_user
    user = update.callback_query.from_user
    if user.id != original_user.id:
        update.callback_query.answer("Non e' il tuo tiro di dadi!")
        return False
    return True

def get_successes(counts):
    """Return Household successes based on dice counts.""" 
    scores = defaultdict(lambda: 0)
    scores.update({2: 1, 3: 3, 4: 6, 5: 10, 6: 15})
    return sum(scores[c] for c in counts.values())

def dice_text(counts, old_counts=None):
    """Pretty formatting for dice results.

    If old_counts is present, it's used to display rerolled dice
    with strikethrough.
    """
    dice_txt = "  ".join(" ".join([str(v)]*c) for v,c in counts.most_common())
    txt = "*dadi:* {}\n".format(dice_txt)
    if old_counts is not None:
        discarded = " ".join([str(v) for v, c in old_counts.items() if c == 1])
        txt += "*scarti:* {}\n".format(discarded)
    if max(counts.values()) >=7:
        txt += "_*MIRACOLO!*_"
    else:
        txt += "*successi:* {}".format(get_successes(counts))
    return txt

def get_keyboard(counts, risked_yet):
    """Get keyboard with "Rischio","Sicurezza","Sto" buttons.

    "Sicurezza" is only available if the player has not risked yet.
    """
    buttons = []
    if len(counts) > 1 and 1 in counts.values():
        # can reroll something
        if max(counts.values()) > 1:
            # can also risk something
            buttons.append(button("Rischio"))
        if not risked_yet:
            buttons.append(button("Sicurezza"))
        if len(buttons) > 0:
            buttons.append(button("Sto"))
    return InlineKeyboardMarkup.from_row(buttons)

def start(update, context):
    txt = "Buonsalve! Usa /roll per tirare i dadi!\n"\
          "Puoi anche specificare il tuo grado:"\
          "`/roll 5` tira direttamente 5 dadi."
    update.message.reply_markdown(txt)

def how_many_dice(update, context):
    """Ask user how many dice should be rolled."""
    kb = InlineKeyboardMarkup.from_row([button(str(i)) for i in range(2,8)])
    update.message.reply_text("Quanti gradi hai?", reply_markup=kb, quote=True)

def roll(update, context):
    """Roll dice if a number was provided, otherwise ask for it."""
    if len(context.args) > 0 and context.args[0].isdigit():
        do_roll(update, context)
    else:
        how_many_dice(update, context)

def do_roll(update, context):
    """Roll dice, communicate result to user together with further options."""
    is_query = update.callback_query is not None
    if is_query:
        if not validate_user(update):
            return
        n_dice = int(update.callback_query.data)
    else:
        # must be a `/roll X` kind of call
        n_dice = int(context.args[0])

    dice_counts = Counter(rand.randint(1, 7, size=n_dice))
    txt = dice_text(dice_counts)
    kb = get_keyboard(dice_counts, risked_yet=False)
    if is_query:
        q = update.callback_query
        q.edit_message_text(txt, reply_markup=kb, parse_mode=MD)
        roll_msg_id = q.message.reply_to_message.message_id
    else:
        # must be a `/roll X` kind of call
        m = update.message
        m.reply_markdown(txt, reply_markup=kb, quote=True)
        roll_msg_id = m.message_id

    context.chat_data[roll_msg_id] = dice_counts

def risk(update, context):
    """Handle Household risk/confidence mechanics."""
    if not validate_user(update):
        return
    q = update.callback_query
    roll_msg_id = q.message.reply_to_message.message_id
    dice_counts = context.chat_data[roll_msg_id]

    old_counts = dice_counts.copy()
    for v, c in old_counts.items():
        if c > 1:
            # keep these dice
            continue
        # remove die to reroll
        if dice_counts[v] == 1:
            del dice_counts[v]
        else:
            dice_counts[v] -= 1
        new_roll = rand.randint(1, 7)
        dice_counts[new_roll] += 1

    txt = dice_text(dice_counts, old_counts)
    old_succ = get_successes(old_counts)
    new_succ = get_successes(dice_counts)
    if new_succ > old_succ:
        diff = new_succ - old_succ
        ending = "i" if diff > 1 else "o"
        txt += "\n\nHai guadagnato +{} success{}!".format(diff, ending)
        kb = get_keyboard(dice_counts, risked_yet=True)
        q.edit_message_text(txt, reply_markup=kb, parse_mode=MD)
        return "rolled"

    # else no new successes
    txt += "\n\nE' andata male!"
    if q.data == "Sicurezza":
        q.edit_message_text(txt, parse_mode=MD)
        return ConversationHandler.END

    # else must discard a die
    txt += " Quale dado vuoi scartare?"
    buttons = [button(v) for v, c in dice_counts.items() if c > 1]
    kb = InlineKeyboardMarkup.from_row(buttons)
    q.edit_message_text(txt, reply_markup=kb, parse_mode=MD)
    return "risk_failed"

def discard(update, context):
    """Discard selected die, communicate final results to user."""
    if not validate_user(update):
        return
    q = update.callback_query
    to_discard = int(q.data)
    roll_msg_id = q.message.reply_to_message.message_id
    dice_counts = context.chat_data[roll_msg_id]
    old_succ = get_successes(dice_counts)
    dice_counts[to_discard] -= 1
    new_succ = get_successes(dice_counts)
    txt = dice_text(dice_counts)
    diff = old_succ - new_succ
    ending = "i" if diff > 1 else "o"
    txt += "\n\nHai perso *{}* success{}!".format(diff, ending)
    q.edit_message_text(txt, parse_mode=MD)
    del context.chat_data[roll_msg_id]
    return ConversationHandler.END

def stand(update, context):
    if not validate_user(update):
        return
    q = update.callback_query
    q.edit_message_reply_markup(reply_markup=None)
    roll_msg_id = q.message.reply_to_message.message_id
    del context.chat_data[roll_msg_id]

def run_bot():
    """Create logger, book bot message handlers, start listening to messages."""
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(format=log_fmt, level=logging.INFO)

    updater = Updater(token='*****', use_context=True)
    d = updater.dispatcher
    d.add_handler(CommandHandler('start', start))
    d.add_handler(CommandHandler('roll', roll))

    risk_hnd = CallbackQueryHandler(risk, pattern="^(Rischio|Sicurezza)$")
    entries = [CallbackQueryHandler(do_roll, pattern="^[2-7]$"), risk_hnd]
    states = {"rolled": [risk_hnd],
              "risk_failed": [CallbackQueryHandler(discard, pattern="^[1-6]$")]}
    conv = ConversationHandler(entries, states, fallbacks=[], per_message=True)
    d.add_handler(conv)
    d.add_handler(CallbackQueryHandler(stand, pattern="^Sto$"))

    updater.start_polling()
    updater.idle()

if __name__ == "__main__":
    run_bot()
